package com.globallogic.duckproject.entity;

import com.globallogic.duckproject.decorator.Label;
import com.globallogic.duckproject.state.State;
import com.globallogic.duckproject.state.StateFactory;

public class Duck implements Cloneable, Label {
    private long duckId;
    private int age;
    private DuckColor color;
    private String name;
    private double weight;
    private double wingslength;
    private static long currentId;
    private State currentState;

    {
        currentId++;
    }

    public enum DuckColor {
        YELLOW, BROWN, GOLD, WHITE;
    }

    public static class DuckBuilder {
        private int age;
        private String name;
        private double wingslength;

        public DuckBuilder age(int val) {
            this.age = val;
            return this;
        }

        public DuckBuilder name(String val) {
            this.name = val;
            return this;
        }

        public DuckBuilder wingsLength(double val) {
            this.wingslength = val;
            return this;
        }

        public Duck build() {
            return new Duck(this);
        }

    }

    Duck(DuckBuilder builder) {
        this.duckId = currentId;
        this.age = builder.age;
        this.name = builder.name;
        this.wingslength = builder.wingslength;
        this.color = setColor(builder.age);
        this.weight = 4 * builder.wingslength;
        this.currentState = StateFactory.STANDING_STATE;
    }

    public Duck(Duck duck) {
        this.duckId = duck.duckId;
        this.age = duck.age;
        this.color = duck.color;
        this.name = duck.name;
        this.weight = duck.weight;
        this.wingslength = duck.wingslength;
        this.currentState = duck.currentState;
    }

    private DuckColor setColor(int age) {
        if (age < 1) {
            return DuckColor.YELLOW;
        } else if(age <= 5) {
            return DuckColor.BROWN;
        } else if(age <= 10) {
            return DuckColor.GOLD;
        } else {
            return DuckColor.WHITE;
        }
    }

    public long getDuckId() {
        return duckId;
    }

    public int getAge() {
        return age;
    }

    public DuckColor getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public double getWingslength() {
        return wingslength;
    }

    @Override
    protected Duck clone() throws CloneNotSupportedException {
        return (Duck) super.clone();
    }

    public Duck getClone() {
        Duck duck = null;
        try {
            duck =  this.clone();
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return duck;
    }

    @Override
    public String toString() {
        return "Duck{" +
            "duckId=" + duckId +
            ", age=" + age +
            ", color=" + color +
            ", name='" + name + '\'' +
            ", weight=" + weight +
            ", wingslength=" + wingslength +
            '}';
    }

    public void setState(State state) {
        this.currentState = state;
    }

    public void fly() {
        try {
            currentState.fly(this);
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }
    }

    public void swim() {
        try {
            currentState.swim(this);
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }
    }

    public void walk() {
        try {
            currentState.walk(this);
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }
    }

    public void run() {
        try {
            currentState.run(this);
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }
    }

    public void stand() {
        try {
            currentState.stand(this);
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }
    }

    public void eat() {
        try {
            currentState.eat(this);
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }
    }

    public void sleep() {
        try {
            currentState.sleep(this);
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }
    }

	@Override
	public void showStatus() {
		System.out.println("A duck. ");
	}
    
    
}