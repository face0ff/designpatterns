package com.globallogic.duckproject.decorator;

public class Tag extends DuckDecorator{
	
	public Tag(Label label) {
		super(label);
	}

	@Override
	public void showStatus() {
		super.showStatus();
		System.out.println("With a tag.");
	}
}