package com.globallogic.duckproject.decorator;

public class Chainlet extends DuckDecorator{
	
	public Chainlet(Label label) {
		super(label);
	}

	@Override
	public void showStatus() {
		super.showStatus();
		System.out.println("With a chainlet.");
	}
	
}
