package com.globallogic.duckproject.decorator;

public abstract class DuckDecorator implements Label{
	
	protected Label label;
	
	public DuckDecorator(Label label) {
		this.label = label;
	}

	@Override
	public void showStatus() {
		this.label.showStatus();
	}
}
