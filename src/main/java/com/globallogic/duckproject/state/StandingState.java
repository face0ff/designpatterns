package com.globallogic.duckproject.state;

import com.globallogic.duckproject.entity.Duck;

public class StandingState implements State {

    @Override
    public void swim(Duck duck) {
        System.out.println("I am swimming!");
        duck.setState(StateFactory.SWIMMING_STATE);
    }

    @Override
    public void walk(Duck duck) {
        System.out.println("I am walking!");
        duck.setState(StateFactory.WALKING_STATE);
    }

    @Override
    public void stand(Duck duck) {
        System.out.println("I am standing!");
    }

    @Override
    public void eat(Duck duck) {
        System.out.println("I am eating!");
        duck.setState(StateFactory.EATING_STATE);
    }

    @Override
    public void sleep(Duck duck) {
        System.out.println("I am sleeping!");
        duck.setState(StateFactory.SLEEPING_STATE);
    }
}
