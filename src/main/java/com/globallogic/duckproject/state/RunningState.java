package com.globallogic.duckproject.state;

import com.globallogic.duckproject.entity.Duck;

public class RunningState implements State {
    @Override
    public void fly(Duck duck) {
        System.out.println("I am flying!");
        duck.setState(StateFactory.FLYING_STATE);
    }

    @Override
    public void walk(Duck duck) {
        System.out.println("I am walking!");
        duck.setState(StateFactory.WALKING_STATE);
    }

    @Override
    public void run(Duck duck) {
        System.out.println("I am running!");
    }

}
