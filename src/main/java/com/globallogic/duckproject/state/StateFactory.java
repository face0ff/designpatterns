package com.globallogic.duckproject.state;

public final class StateFactory {
    public static final EatingState EATING_STATE = new EatingState();
    public static final FlyingState FLYING_STATE = new FlyingState();
    public static final RunningState RUNNING_STATE = new RunningState();
    public static final SleepingState SLEEPING_STATE = new SleepingState();
    public static final StandingState STANDING_STATE = new StandingState();
    public static final SwimmingState SWIMMING_STATE = new SwimmingState();
    public static final WalkingState WALKING_STATE = new WalkingState();

    private StateFactory() {}
}
