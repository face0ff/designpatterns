package com.globallogic.duckproject.state;

import com.globallogic.duckproject.entity.Duck;

public class WalkingState implements State {

    @Override
    public void swim(Duck duck) {
        System.out.println("I am swimming!");
        duck.setState(StateFactory.SWIMMING_STATE);
    }

    @Override
    public void walk(Duck duck) {
        System.out.println("I am walking!");
    }

    @Override
    public void run(Duck duck) {
        System.out.println("I am running!");
        duck.setState(StateFactory.RUNNING_STATE);
    }

    @Override
    public void stand(Duck duck) {
        System.out.println("I am standing!");
        duck.setState(StateFactory.STANDING_STATE);
    }
}
