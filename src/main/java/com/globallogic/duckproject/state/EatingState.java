package com.globallogic.duckproject.state;

import com.globallogic.duckproject.entity.Duck;

public class EatingState implements State {

    @Override
    public void stand(Duck duck) {
        System.out.println("I am standing!");
        duck.setState(StateFactory.STANDING_STATE);
    }

    @Override
    public void eat(Duck duck) {
        System.out.println("I am eating!");
    }
}
